package com.gregorius.kuufproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity
{
    public static final String KEY_LOGGED_USER_ID = "userID";

    TextView tvMessage;
    TextView tvUsername, tvBalance;
    RecyclerView rvHistory;
    TransactionsTableHelper transactionsTableHelper;
    ArrayList<HistoryItem> listTransaction;
    UserTransactionAdapter userTransactionAdapter;

    UsersTableHelper usersTableHelper;
    User user;

    int userID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        tvMessage = findViewById(R.id.tvMessage);
        tvUsername = findViewById(R.id.tvUsername);
        tvBalance = findViewById(R.id.tvBalance);
        rvHistory = findViewById(R.id.rvHistory);

        transactionsTableHelper = new TransactionsTableHelper(this);
        LinearLayoutManager llManager = new LinearLayoutManager(this);
        rvHistory.setLayoutManager(llManager);

        usersTableHelper = new UsersTableHelper(this);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        userID = sp.getInt(KEY_LOGGED_USER_ID, -1);
        user = usersTableHelper.getById(userID);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        resetBuyStatus();
        refreshData();
        updateWallet();
    }

    private void updateWallet()
    {
        usersTableHelper = new UsersTableHelper(this);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        userID = sp.getInt(KEY_LOGGED_USER_ID, -1);
        user = usersTableHelper.getById(userID);
        tvUsername.setText(user.getUsername());
        tvBalance.setText(formattedNominal(user.getWallet()));
    }

    public void resetBuyStatus()
    {
        SharedPreferences.Editor sPrefs = PreferenceManager.getDefaultSharedPreferences(this).edit();
        sPrefs.putInt(StoreActivity.KEY_BUY_STATUS, 0);
        sPrefs.apply();
    }

    private void refreshData()
    {

        if (transactionsTableHelper.count(userID) == 0) onListEmpty();
        else
        {
            tvMessage.setVisibility(TextView.INVISIBLE);
            rvHistory.setVisibility(RecyclerView.VISIBLE);
            listTransaction = transactionsTableHelper.getUserHistory(userID);
            userTransactionAdapter = new UserTransactionAdapter(this, listTransaction);
            userTransactionAdapter.setOnItemClickListener(new UserTransactionAdapter.ClickListener()
            {
                @Override
                public void onItemClick()
                {
                    onListEmpty();
                }
            });
            rvHistory.setAdapter(userTransactionAdapter);
        }
    }

    private void onListEmpty()
    {
        tvMessage.setText("There is no transaction");
        tvMessage.setVisibility(TextView.VISIBLE);
        rvHistory.setVisibility(RecyclerView.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        Intent intent = null;

        switch (item.getItemId())
        {
            case R.id.menuHome:
                Toast.makeText(HomeActivity.this, "You are home!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menuStore:
                intent = new Intent(HomeActivity.this, StoreActivity.class);
                break;
            case R.id.menuProfile:
                intent = new Intent(HomeActivity.this, ProfileActivity.class);
                break;
            case R.id.menuLogout:
                SharedPreferences.Editor sp = PreferenceManager.getDefaultSharedPreferences(this).edit();
                sp.remove(KEY_LOGGED_USER_ID);
                sp.apply();

                intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
                startActivity(intent);
                break;
        }
        if (intent != null)
        {
            startActivity(intent);
//			finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static String formattedNominal(long nominal)
    {
        String temp = Long.toString(nominal);

        int i, length = temp.length();

        String nominalString = "Rp";
        nominalString += temp.charAt(0);

        for(i=1; i<length; i++)
        {
            if(i%3 == length%3) nominalString += ".";
            nominalString += temp.charAt(i);
        }

        nominalString += ",00";

        return nominalString;
    }

}
