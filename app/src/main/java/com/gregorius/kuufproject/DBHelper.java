package com.gregorius.kuufproject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "Kuuf";

	public static final String TABLE_USERS = "Users";
	public static final String USERS_FIELD_ID = "id";
	public static final String USERS_FIELD_USERNAME = "username";
	public static final String USERS_FIELD_PASSWORD = "password";
	public static final String USERS_FIELD_PHONE = "phone";
	public static final String USERS_FIELD_GENDER = "gender";
	public static final String USERS_FIELD_WALLET = "wallet";
	public static final String USERS_FIELD_DATEOFBIRTH = "dateOfBirth";

	public static final String TABLE_PRODUCTS = "Products";
	public static final String PRODUCTS_FIELD_ID = "id";
	public static final String PRODUCTS_FIELD_NAME = "name";
	public static final String PRODUCTS_FIELD_MINPLAYER = "minPlayer";
	public static final String PRODUCTS_FIELD_MAXPLAYER = "maxPlayer";
	public static final String PRODUCTS_FIELD_PRICE = "price";
	public static final String PRODUCTS_FIELD_LATITUDE = "latitude";
	public static final String PRODUCTS_FIELD_LONGITUDE = "longitude";

	public static final String TABLE_TRANSACTIONS = "Transactions";
	public static final String TRANSACTIONS_FIELD_ID = "id";
	public static final String TRANSACTIONS_FIELD_USERID = "userId";
	public static final String TRANSACTIONS_FIELD_PRODUCTID = "productId";
	public static final String TRANSACTIONS_FIELD_TRANSACTIONDATE = "transactionDate";

	public DBHelper(Context context) {
		super(context, DB_NAME, null, 2);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String query1 = String.format("CREATE TABLE IF NOT EXISTS %s(" +
				"%s INTEGER PRIMARY KEY AUTOINCREMENT," +
				"%s TEXT NOT NULL," +
				"%s TEXT NOT NULL," +
				"%s TEXT NOT NULL," +
				"%s TEXT NOT NULL," +
				"%s INTEGER NOT NULL," +
				"%s TEXT NOT NULL" +
				");", TABLE_USERS, USERS_FIELD_ID,
				USERS_FIELD_USERNAME, USERS_FIELD_PASSWORD,
				USERS_FIELD_PHONE, USERS_FIELD_GENDER,
				USERS_FIELD_WALLET, USERS_FIELD_DATEOFBIRTH
		);
		String query2 = String.format("CREATE TABLE IF NOT EXISTS %s (" +
				"%s INTEGER PRIMARY KEY AUTOINCREMENT," +
				"%s TEXT NOT NULL," +
				"%s INTEGER NOT NULL," +
				"%s INTEGER NOT NULL," +
				"%s INTEGER NOT NULL," +
				"%s REAL NOT NULL," +
				"%s REAL NOT NULL" +
				");", TABLE_PRODUCTS, PRODUCTS_FIELD_ID,
				PRODUCTS_FIELD_NAME, PRODUCTS_FIELD_MINPLAYER,
				PRODUCTS_FIELD_MAXPLAYER, PRODUCTS_FIELD_PRICE,
				PRODUCTS_FIELD_LATITUDE, PRODUCTS_FIELD_LONGITUDE
		);
		String query3 = String.format("CREATE TABLE IF NOT EXISTS %s (" +
				"%s INTEGER PRIMARY KEY AUTOINCREMENT," +
				"%s INTEGER NOT NULL," +
				"%s INTEGER NOT NULL," +
				"%s TEXT NOT NULL," +
				"FOREIGN KEY (%s) REFERENCES Users (%s) " +
				"ON DELETE CASCADE ON UPDATE CASCADE," +
				"FOREIGN KEY (%s) REFERENCES Products (%s) " +
				"ON DELETE CASCADE ON UPDATE CASCADE" +
				");", TABLE_TRANSACTIONS, TRANSACTIONS_FIELD_ID,
				TRANSACTIONS_FIELD_USERID, TRANSACTIONS_FIELD_PRODUCTID,
				TRANSACTIONS_FIELD_TRANSACTIONDATE,
				TRANSACTIONS_FIELD_USERID, USERS_FIELD_ID,
				TRANSACTIONS_FIELD_PRODUCTID, PRODUCTS_FIELD_ID
		);

		db.execSQL(query1);
		db.execSQL(query2);
		db.execSQL(query3);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String query1 = String.format("DROP TABLE IF EXISTS %s", TABLE_TRANSACTIONS);
		String query2 = String.format("DROP TABLE IF EXISTS %s", TABLE_PRODUCTS);
		String query3 = String.format("DROP TABLE IF EXISTS %s", TABLE_USERS);

		db.execSQL(query1);
		db.execSQL(query2);
		db.execSQL(query3);

		onCreate(db);
	}
}
