package com.gregorius.kuufproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class UserTransactionAdapter extends RecyclerView.Adapter<UserTransactionAdapter.ViewHolder> {

	protected Context ctx;
	private ArrayList<HistoryItem> listTransaction;
	TransactionsTableHelper transactionsTableHelper;
	private ClickListener clickListener;

	public UserTransactionAdapter(Context ctx, ArrayList<HistoryItem> listTransaction) {
		this.ctx = ctx;
		this.setListTransaction(listTransaction);
		transactionsTableHelper = new TransactionsTableHelper(ctx);
	}

	@NonNull
	@Override
	public UserTransactionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(ctx).inflate(R.layout.item_row_history, parent, false);
		return new UserTransactionAdapter.ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(@NonNull UserTransactionAdapter.ViewHolder holder, int position) {
		HistoryItem item = getListTransaction().get(position);
		holder.tvTransactionID.setText(String.valueOf(item.getId()));
		holder.tvProductName.setText(item.getName());
		holder.tvPrice.setText("Price: " + HomeActivity.formattedNominal(item.getPrice()));
		holder.tvTransactionDate.setText("Transaction Date: " + item.getTransactionDate());
		holder.btnDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				transactionsTableHelper.removeById(Integer.parseInt(Integer.toString(item.getId())));
				listTransaction.remove(position);
				notifyItemRemoved(position);
				notifyItemRangeChanged(position, listTransaction.size());
				if(listTransaction.size() == 0) clickListener.onItemClick();
			}
		});
	}

	@Override
	public int getItemCount() {
		return listTransaction.size();
	}

	public ArrayList<HistoryItem> getListTransaction() {
		return listTransaction;
	}

	public void setListTransaction(ArrayList<HistoryItem> listTransaction) {
		this.listTransaction = listTransaction;
	}

	class ViewHolder extends RecyclerView.ViewHolder{
		TextView tvTransactionID;
		TextView tvProductName;
		TextView tvPrice;
		TextView tvTransactionDate;
		Button btnDelete;

		public ViewHolder(@NonNull View itemView) {
			super(itemView);

			tvTransactionID = itemView.findViewById(R.id.tvTransactionID);
			tvProductName = itemView.findViewById(R.id.tvProductName);
			tvPrice = itemView.findViewById(R.id.tvPrice);
			tvTransactionDate = itemView.findViewById(R.id.tvTransactionDate);
			btnDelete = itemView.findViewById(R.id.btnDelete);
		}
	}

	public void setOnItemClickListener(ClickListener clickListener) {
		this.clickListener = clickListener;
	}

	public interface ClickListener {
		void onItemClick();
	}
}
