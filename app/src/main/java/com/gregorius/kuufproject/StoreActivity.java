package com.gregorius.kuufproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class StoreActivity extends AppCompatActivity {

	RecyclerView rvStore;
	StoreAdapter storeAdapter;
	ArrayList<Product> listProduct;
	ProductsTableHelper productsTableHelper;

	TextView tvMessage;

	private static final String API_URL = "https://api.jsonbin.io/b/5eb51c6947a2266b1474d701/7";
	public static final String KEY_BUY_STATUS = "BUY_STATUS" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

		rvStore = findViewById(R.id.rvStore);
		productsTableHelper = new ProductsTableHelper(this);

		if (productsTableHelper.count() == 0) {
			Log.i("MY_TAG", "masuk");
			fetchData();
		}

		listProduct = productsTableHelper.getAll();

		// Toast.makeText(StoreActivity.this, ">" + listProduct.size(), Toast.LENGTH_SHORT).show();
		LinearLayoutManager llManager = new LinearLayoutManager(this);
		rvStore.setLayoutManager(llManager);

		storeAdapter = new StoreAdapter(this, listProduct);
		rvStore.setAdapter(storeAdapter);
    }

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		int buyStatus = sPrefs.getInt(KEY_BUY_STATUS, -1);
		if (buyStatus == 1) finish();
	}

	private void fetchData() {

		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
				(Request.Method.GET, API_URL, null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.i("MY_TAG", "Response: " + response.toString());
						// tvMessage.setText("Response: " + response.toString());
						bulkInsert(response);

						listProduct = productsTableHelper.getAll();
						storeAdapter.setListProduct(listProduct);
						storeAdapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO: Handle error

					}
				});

		SingletonRequestQueue.getInstance(this).getRequestQueue().add(jsonObjectRequest);
	}

	private void bulkInsert(JSONObject response) {
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject(response.toString());

			JSONArray items = jsonObj.getJSONArray("items");

			for (int i = 0; i < items.length(); i++) {
				JSONObject item = items.getJSONObject(i);

				String name = item.getString("name");
				int min_player = item.getInt("min_player");
				int max_player = item.getInt("max_player");
				int price = item.getInt("price");
				String created_at = item.getString("created_at");
				Double latitude = item.getDouble("latitude");
				Double longitude = item.getDouble("longitude");

				Product product = new Product(-1, name, min_player, max_player, price, latitude, longitude);

				productsTableHelper.insert(product);
				// Log.i("MY_TAG2", "bulkInsert: " + product.getName());

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
