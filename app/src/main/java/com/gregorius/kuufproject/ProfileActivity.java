package com.gregorius.kuufproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity
{

    UsersTableHelper usersTableHelper;
    User user;

    TextView tvUsername;
    TextView tvGender;
    TextView tvPhone;
    TextView tvWallet;
    TextView tvDOB;
    RadioGroup rgNominals;
    EditText etPassword;
    Button btnConfirm;

    long selectedNominal = 0;
    long oldNominal = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        usersTableHelper = new UsersTableHelper(this);

        tvUsername = findViewById(R.id.tvUsername);
        tvGender = findViewById(R.id.tvGender);
        tvPhone = findViewById(R.id.tvPhone);
        tvWallet = findViewById(R.id.tvWallet);
        tvDOB = findViewById(R.id.tvDOB);
        rgNominals = findViewById(R.id.rgNominals);
        etPassword = findViewById(R.id.etPassword);
        btnConfirm = findViewById(R.id.btnConfirm);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this);
        int userID = sp.getInt(HomeActivity.KEY_LOGGED_USER_ID, -1);
        user = usersTableHelper.getById(userID);

        if (user != null)
        {
            oldNominal = user.getWallet();

            tvUsername.setText("Username: " + user.getUsername());
            tvGender.setText("Gender: " + user.getGender());
            tvPhone.setText("Phone: " + user.getPhone());
            tvWallet.setText("Wallet: " + HomeActivity.formattedNominal(oldNominal));
            tvDOB.setText("Date of Birth: " + user.getDateOfBirth());
        }

        rgNominals.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch (checkedId)
                {
                    case R.id.rbNominalVal1:
                        selectedNominal = 250000;
                        break;
                    case R.id.rbNominalVal2:
                        selectedNominal = 500000;
                        break;
                    case R.id.rbNominalVal3:
                        selectedNominal = 1000000;
                        break;
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                boolean valid = true;
                String password = etPassword.getText().toString();

                if (selectedNominal == 0)
                {
                    Toast.makeText(ProfileActivity.this, "Please select the nominal you want!", Toast.LENGTH_SHORT).show();
                    valid = false;
                }

                if (Validation.isEmpty(password))
                {
                    etPassword.setError("Password cannot empty!");
                    valid = false;
//                    Toast.makeText(ProfileActivity.this, "Password cannot empty!", Toast.LENGTH_SHORT).show();
                }
                else if (!password.equals(user.getPassword()))
                {
                    etPassword.setError("Password is wrong!");
//                    Toast.makeText(ProfileActivity.this, "Password is wrong!", Toast.LENGTH_SHORT).show();
                    valid = false;
                }

                if (valid)
                {
                    long newNominal = oldNominal + selectedNominal;
                    Log.v("MYLOG", String.valueOf(newNominal));
                    usersTableHelper.updateWallet(user.getId(), newNominal);
                    Toast.makeText(ProfileActivity.this, "Wallet top up successful!", Toast.LENGTH_SHORT).show();

//					Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
//					startActivity(intent);
                    finish();
                }
            }
        });
    }
}
