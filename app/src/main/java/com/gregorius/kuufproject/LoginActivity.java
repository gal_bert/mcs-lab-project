package com.gregorius.kuufproject;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity
{

    EditText etUsername;
    EditText etPassword;
    Button btnLogin;
    Button btnRegister;

    UsersTableHelper usersTableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.v("MYLOG", "Created");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);

        usersTableHelper = new UsersTableHelper(this);

        btnLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                boolean valid = true;
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                if (Validation.isEmpty(username))
                {
                    etUsername.setError("Name cannot be empty!");
                    valid = false;
//					Toast.makeText(LoginActivity.this, "Name cannot empty!", Toast.LENGTH_SHORT).show();
                }
                if (Validation.isEmpty(password))
                {
                    etPassword.setError("Password cannot be empty!");
                    valid = false;
//					Toast.makeText(LoginActivity.this, "Password cannot empty!", Toast.LENGTH_SHORT).show();
                }
                if (valid)
                {
                    User user = usersTableHelper.login(username, password);

                    if (user == null)
                    {
                        Toast.makeText(LoginActivity.this, "Username or password is wrong!", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt(HomeActivity.KEY_LOGGED_USER_ID, user.getId());
                        editor.apply();

                        goToHome();
                    }
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        if(sharedPreferences.getInt(HomeActivity.KEY_LOGGED_USER_ID, -1) != -1) goToHome();
    }

    private void goToHome()
    {
        Intent intent = new Intent(this, HomeActivity.class);
        finish();
        startActivity(intent);
    }
}
