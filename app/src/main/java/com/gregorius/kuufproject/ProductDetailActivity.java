package com.gregorius.kuufproject;

import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductDetailActivity extends AppCompatActivity
{

    public static final String KEY_PRODUCT_ID = "productID";
    public static final String KEY_LATITUDE = "lat";
    public static final String KEY_LONGITUDE = "lon";

    TextView tvProductID;
    TextView tvProductName;
    TextView tvMinPlayer;
    TextView tvMaxPlayer;
    TextView tvPrice;
    Button btnShowLocation;
    Button btnBuy;

    UsersTableHelper usersTableHelper = new UsersTableHelper(this);
    ProductsTableHelper productsTableHelper = new ProductsTableHelper(this);
    TransactionsTableHelper transactionsTableHelper = new TransactionsTableHelper(this);

    SmsManager smsManager;

    Product product;
    User user;
    String createdDate;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        tvProductID = findViewById(R.id.tvProductID);
        tvProductName = findViewById(R.id.tvProductName);
        tvMinPlayer = findViewById(R.id.tvMinPlayer);
        tvMaxPlayer = findViewById(R.id.tvMaxPlayer);
        tvPrice = findViewById(R.id.tvPrice);
        btnShowLocation = findViewById(R.id.btnShowLocation);
        btnBuy = findViewById(R.id.btnBuy);

        int productID = getIntent().getIntExtra(KEY_PRODUCT_ID, -1);
        product = productsTableHelper.getById(productID);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ProductDetailActivity.this);
        int userID = sp.getInt(HomeActivity.KEY_LOGGED_USER_ID, -1);
        user = usersTableHelper.getById(userID);

        if (product != null)
        {
            tvProductID.setText(String.valueOf(product.getId()));
            tvProductName.setText(product.getName());
            tvMinPlayer.setText("Min Player: " + product.getMinPlayer());
            tvMaxPlayer.setText("Max Player: " + product.getMaxPlayer());
            tvPrice.setText("Price: " + HomeActivity.formattedNominal(product.getPrice()));
        }

        btnBuy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (user.getWallet() < product.getPrice())
                {
                    Toast.makeText(ProductDetailActivity.this, "Your wallet balance is not enough!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    requestPermission();
                    if (ActivityCompat.checkSelfPermission(ProductDetailActivity.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
                        Toast.makeText(ProductDetailActivity.this, "Please accept the SEND_SMS permission", Toast.LENGTH_LONG).show();
                    else
                    {
                        transactionsTableHelper.insert(new Transaction(-1, user.getId(), product.getId(), getCurrentDate()));
                        usersTableHelper.updateWallet(user.getId(), user.getWallet() - product.getPrice());

                        SharedPreferences.Editor sPrefs = PreferenceManager.getDefaultSharedPreferences(ProductDetailActivity.this).edit();
                        sPrefs.putInt(StoreActivity.KEY_BUY_STATUS, 1);
                        sPrefs.apply();
                        finish();

                        sendSms();

                        Toast.makeText(ProductDetailActivity.this, "Purchase successful!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnShowLocation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ProductDetailActivity.this, MapsActivity.class);
                intent.putExtra(KEY_LATITUDE, product.getLatitude());
                intent.putExtra(KEY_LONGITUDE, product.getLongitude());
                startActivity(intent);
            }
        });
    }

    private String getCurrentDate()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return formatter.format(date);
    }

    private void requestPermission()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
        {
            String[] strings = {Manifest.permission.SEND_SMS};
            ActivityCompat.requestPermissions(this, strings, 1);
        }
    }

    @RequiresPermission(Manifest.permission.SEND_SMS)
    private void sendSms()
    {
        String message = user.getUsername() + ", your transaction is success. Product: " + product.getName() + ". Price: " + HomeActivity.formattedNominal(product.getPrice()) + ".";
        smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(user.getPhone(), null, message, null, null);
    }
}
