package com.gregorius.kuufproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductsTableHelper {
	private DBHelper dbHelper;

	public ProductsTableHelper(Context ctx) {
		dbHelper = new DBHelper(ctx);
	}

	public void insert(Product data) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(DBHelper.PRODUCTS_FIELD_NAME, data.getName());
		cv.put(DBHelper.PRODUCTS_FIELD_MINPLAYER, data.getMinPlayer());
		cv.put(DBHelper.PRODUCTS_FIELD_MAXPLAYER, data.getMaxPlayer());
		cv.put(DBHelper.PRODUCTS_FIELD_PRICE, data.getPrice());
		cv.put(DBHelper.PRODUCTS_FIELD_LATITUDE, data.getLatitude());
		cv.put(DBHelper.PRODUCTS_FIELD_LONGITUDE, data.getLongitude());

		db.insert(DBHelper.TABLE_PRODUCTS, null, cv);
		db.close();
	}

	public int count() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String[] column = {"COUNT(*)"};

		Cursor cursor = db.query(DBHelper.TABLE_PRODUCTS, column, null,null, null, null, null);
//		Cursor cursor = db.rawQuery(String.format("SELECT COUNT(*) FROM %s", DBHelper.TABLE_PRODUCTS), null);
		cursor.moveToFirst();

		int count = cursor.getInt(0);

		cursor.close();
		db.close();
		dbHelper.close();
		return count;
	}

	public ArrayList<Product> getAll() {
		ArrayList<Product> listProduct = new ArrayList<>();

		SQLiteDatabase db = dbHelper.getReadableDatabase();
//		String query = String.format("SELECT * FROM %s", DBHelper.TABLE_PRODUCTS);
//		Cursor cursor = db.rawQuery(query, null);
		Cursor cursor = db.query(DBHelper.TABLE_PRODUCTS, null, null, null, null, null, null);
		cursor.moveToFirst();

		if (cursor.getCount() > 0) {
			while (!cursor.isAfterLast()) {
				int id = cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_ID));
				String name = cursor.getString(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_NAME));
				int minPlayer = cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_MINPLAYER));
				int maxPlayer = cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_MAXPLAYER));
				int price = cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_PRICE));
				double latitude = cursor.getDouble(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_LATITUDE));
				double longitude = cursor.getDouble(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_LONGITUDE));

				listProduct.add(new Product(id, name, minPlayer, maxPlayer, price, latitude, longitude));

				cursor.moveToNext();
			}
		}

		cursor.close();
		db.close();
		dbHelper.close();
		return listProduct;
	}

	public Product getById(int id) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = db.query(
				DBHelper.TABLE_PRODUCTS,
				null,
				DBHelper.PRODUCTS_FIELD_ID + "= ?",
				new String[]{String.valueOf(id)},
				null, null, null
		);

		Product product = null;

		if(cursor.moveToFirst()) {
			product = new Product(
					cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_ID)),
					cursor.getString(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_NAME)),
					cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_MINPLAYER)),
					cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_MAXPLAYER)),
					cursor.getInt(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_PRICE)),
					cursor.getDouble(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_LATITUDE)),
					cursor.getDouble(cursor.getColumnIndex(DBHelper.PRODUCTS_FIELD_LONGITUDE))
			);
		}

		cursor.close();
		db.close();
		dbHelper.close();
		return product;
	}
}
