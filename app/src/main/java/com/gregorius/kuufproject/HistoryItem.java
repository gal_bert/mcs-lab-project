package com.gregorius.kuufproject;

public class HistoryItem {
	private int id;
	private String name;
	private int price;
	private String transactionDate;

	public HistoryItem(int id, String name, int price, String transactionDate)
	{
		this.id = id;
		this.name = name;
		this.price = price;
		this.transactionDate = transactionDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
