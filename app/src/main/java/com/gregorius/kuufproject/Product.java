package com.gregorius.kuufproject;

public class Product {
	private int id;
	private String name;
	private int minPlayer;
	private int maxPlayer;
	private long price;
	private double latitude;
	private double longitude;

	public Product(int id, String name, int minPlayer, int maxPlayer, long price, double latitude, double longitude)
	{
		this.id = id;
		this.name = name;
		this.minPlayer = minPlayer;
		this.maxPlayer = maxPlayer;
		this.price = price;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMinPlayer() {
		return minPlayer;
	}

	public void setMinPlayer(int minPlayer) {
		this.minPlayer = minPlayer;
	}

	public int getMaxPlayer() {
		return maxPlayer;
	}

	public void setMaxPlayer(int maxPlayer) {
		this.maxPlayer = maxPlayer;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
