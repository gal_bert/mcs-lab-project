package com.gregorius.kuufproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class UsersTableHelper {
	private DBHelper dbHelper;

	public UsersTableHelper(Context ctx) {
		dbHelper = new DBHelper(ctx);
	}

	public void insert(User data) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(DBHelper.USERS_FIELD_USERNAME, data.getUsername());
		cv.put(DBHelper.USERS_FIELD_PASSWORD, data.getPassword());
		cv.put(DBHelper.USERS_FIELD_PHONE, data.getPhone());
		cv.put(DBHelper.USERS_FIELD_GENDER, data.getGender());
		cv.put(DBHelper.USERS_FIELD_WALLET, data.getWallet());
		cv.put(DBHelper.USERS_FIELD_DATEOFBIRTH, data.getDateOfBirth());

		db.insert(DBHelper.TABLE_USERS, null, cv);
		db.close();
	}

	public User login(String username, String password) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String selection = DBHelper.USERS_FIELD_USERNAME +"= ? AND " + DBHelper.USERS_FIELD_PASSWORD + "= ?";
		String[] selectionArgs = {username, password};
		Cursor cursor = db.query(DBHelper.TABLE_USERS, null, selection, selectionArgs, null, null, null);

		User user = null;

		if(cursor.moveToFirst()) {
			user = new User(
				cursor.getInt(cursor.getColumnIndex(DBHelper.USERS_FIELD_ID)),
				cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_USERNAME)),
				cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_PASSWORD)),
				cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_PHONE)),
				cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_GENDER)),
				cursor.getInt(cursor.getColumnIndex(DBHelper.USERS_FIELD_WALLET)),
				cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_DATEOFBIRTH))
			);
		}

		cursor.close();
		db.close();
		dbHelper.close();
		return user;
	}

	public User getById(int id) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String selection = DBHelper.USERS_FIELD_ID +"= ?";
		String[] selectionArgs = {String.valueOf(id)};

		Cursor cursor = db.query(DBHelper.TABLE_USERS, null, selection, selectionArgs, null, null, null);

		User user = null;

		if(cursor.moveToFirst()) {
			user = new User(
					cursor.getInt(cursor.getColumnIndex(DBHelper.USERS_FIELD_ID)),
					cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_USERNAME)),
					cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_PASSWORD)),
					cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_PHONE)),
					cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_GENDER)),
					cursor.getInt(cursor.getColumnIndex(DBHelper.USERS_FIELD_WALLET)),
					cursor.getString(cursor.getColumnIndex(DBHelper.USERS_FIELD_DATEOFBIRTH))
			);
		}

		cursor.close();
		db.close();
		dbHelper.close();
		return user;
	}

	public void updateWallet(int userID, long nominal) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		String selection = DBHelper.USERS_FIELD_ID +"= ?";
		String[] selectionArgs = {String.valueOf(userID)};

		ContentValues cv = new ContentValues();
		cv.put(DBHelper.USERS_FIELD_WALLET, nominal);
		db.update(DBHelper.TABLE_USERS, cv, selection, selectionArgs);
		db.close();
		dbHelper.close();
	}
}
