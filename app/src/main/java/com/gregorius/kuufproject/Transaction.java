package com.gregorius.kuufproject;

public class Transaction {
	private int id;
	private int userId;
	private int productId;
	private String transactionDate;

	public Transaction(int id, int userId, int productId, String transactionDate)
	{
		this.id = id;
		this.userId = userId;
		this.productId = productId;
		this.transactionDate = transactionDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
}
