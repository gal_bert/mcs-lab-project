package com.gregorius.kuufproject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {

	protected Context ctx;
	private ArrayList<Product> listProduct;

	public StoreAdapter(Context ctx, ArrayList<Product> listProduct) {
		this.ctx = ctx;
		this.setListProduct(listProduct);
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(ctx).inflate(R.layout.item_row_store, parent, false);
		return new ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(@NonNull StoreAdapter.ViewHolder holder, int position) {
		Product product = getListProduct().get(position);
		holder.tvProductID.setText(String.valueOf(product.getId()));
		holder.tvProductName.setText(product.getName());
		holder.tvMinPlayer.setText("Min Player: " + product.getMinPlayer());
		holder.tvMaxPlayer.setText("Max Player: " + product.getMaxPlayer());
		holder.tvPrice.setText("Price: " + HomeActivity.formattedNominal(product.getPrice()));
	}

	@Override
	public int getItemCount() {
		return getListProduct().size();
	}

	public ArrayList<Product> getListProduct() {
		return listProduct;
	}

	public void setListProduct(ArrayList<Product> listProduct) {
		this.listProduct = listProduct;
		notifyDataSetChanged();
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		TextView tvProductID;
		TextView tvProductName;
		TextView tvMinPlayer;
		TextView tvMaxPlayer;
		TextView tvPrice;

		public ViewHolder(@NonNull View itemView) {
			super(itemView);
			tvProductID = itemView.findViewById(R.id.tvProductID);
			tvProductName = itemView.findViewById(R.id.tvProductName);
			tvMinPlayer = itemView.findViewById(R.id.tvMinPlayer);
			tvMaxPlayer = itemView.findViewById(R.id.tvMaxPlayer);
			tvPrice = itemView.findViewById(R.id.tvPrice);

			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(ctx, ProductDetailActivity.class);
					intent.putExtra(ProductDetailActivity.KEY_PRODUCT_ID, Integer.parseInt(tvProductID.getText().toString()));
					ctx.startActivity(intent);
				}
			});
		}
	}
}
