package com.gregorius.kuufproject;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public class TransactionsTableHelper {

	private DBHelper dbHelper;

	public TransactionsTableHelper(Context ctx) {
		dbHelper = new DBHelper(ctx);
	}

	public void insert(Transaction data) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues cv = new ContentValues();

		cv.put(DBHelper.TRANSACTIONS_FIELD_USERID, data.getUserId());
		cv.put(DBHelper.TRANSACTIONS_FIELD_PRODUCTID, data.getProductId());
		cv.put(DBHelper.TRANSACTIONS_FIELD_TRANSACTIONDATE, data.getTransactionDate());

		db.insert(DBHelper.TABLE_TRANSACTIONS, null, cv);
		db.close();
	}

	public ArrayList<HistoryItem> getUserHistory(int userID) {
		ArrayList<HistoryItem> listTransaction = new ArrayList<>();

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String query = "SELECT t.id AS transactionID, p.name AS productName, p.price AS productPrice, t.transactionDate FROM Transactions t INNER JOIN Users u ON t.userId = u.id INNER JOIN Products p ON t.productId = p.id WHERE u.id = " + userID + ";";
		Cursor cursor = db.rawQuery(query, null);
		cursor.moveToFirst();

		if (cursor.getCount() > 0) {
			while (!cursor.isAfterLast()) {
				listTransaction.add(new HistoryItem(
						cursor.getInt(cursor.getColumnIndex("transactionID")),
						cursor.getString(cursor.getColumnIndex("productName")),
						cursor.getInt(cursor.getColumnIndex("productPrice")),
						cursor.getString(cursor.getColumnIndex("transactionDate"))
				));

				cursor.moveToNext();
			}
		}

		cursor.close();
		db.close();
		dbHelper.close();
		return listTransaction;
	}

	public void removeById(int id) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		db.delete(DBHelper.TABLE_TRANSACTIONS, "id = ?", new String[]{String.valueOf(id)});
		db.close();
		dbHelper.close();
	}

	public int count(int id) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String[] column = {"COUNT(*)"};
		String selection = DBHelper.TRANSACTIONS_FIELD_USERID + "=?";
		String[] selectionArgs = {String.valueOf(id)};
		Cursor cursor = db.query(DBHelper.TABLE_TRANSACTIONS, column, selection, selectionArgs, null, null, null);
//		Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM Transactions WHERE userId = ?", selectionArgs, null);
		cursor.moveToFirst();

		int count = cursor.getInt(0);

		cursor.close();
		db.close();
		dbHelper.close();
		return count;
	}
}
