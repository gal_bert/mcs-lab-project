package com.gregorius.kuufproject;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Date;

public class RegisterActivity extends AppCompatActivity
{
    Date dateOfBirth;
    EditText etUsername;
    EditText etPassword;
    EditText etPhone;
    EditText etConfPassword;
    EditText etDOB;
    RadioGroup rgGender;
    CheckBox cbAgreement;
    Button btnOpenDateDialog;
    Button btnRegister;
    Button btnLogin;

    String selectedGender = "";

    UsersTableHelper usersTableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        etPhone = findViewById(R.id.etPhone);
        etConfPassword = findViewById(R.id.etConfPassword);
        etDOB = findViewById(R.id.etDOB);
        rgGender = findViewById(R.id.rgGender);
        cbAgreement = findViewById(R.id.cbAgreement);
        btnRegister = findViewById(R.id.btnRegister);
        btnLogin = findViewById(R.id.btnLogin);
        btnOpenDateDialog = findViewById(R.id.btnOpenDateDialog);

        usersTableHelper = new UsersTableHelper(this);

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch (checkedId)
                {
                    case R.id.rbMale:
                        selectedGender = "Male";
                        break;
                    case R.id.rbFemale:
                        selectedGender = "Female";
                        break;
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                boolean valid = true;
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String phone = etPhone.getText().toString();
                String confPassword = etConfPassword.getText().toString();
                String dob = etDOB.getText().toString();

                if (Validation.isEmpty(username))
                {
                    etUsername.setError("Name cannot be empty!");
                    valid = false;
//					Toast.makeText(RegisterActivity.this, "Name cannot empty!", Toast.LENGTH_SHORT).show();
                }
                else if (!Validation.usernameLength(username))
                {
                    etUsername.setError("Username must be between 6 and 12 characters!");
                    valid = false;
//                    Toast.makeText(RegisterActivity.this, "Username must be between 6 and 12 characters!", Toast.LENGTH_SHORT).show();
                }

                if (Validation.isEmpty(password))
                {
                    etPassword.setError("Password cannot be empty!");
                    valid = false;
//					Toast.makeText(RegisterActivity.this, "Password cannot empty!", Toast.LENGTH_SHORT).show();
                }
                else if (!Validation.passwordLength(password))
                {
                    etPassword.setError("Password must be more than 8 characters!");
                    valid = false;
//                    Toast.makeText(RegisterActivity.this, "Password must be more than 8 characters!", Toast.LENGTH_SHORT).show();
                }
                else if (!Validation.isAlphanumeric(password))
                {
                    etPassword.setError("Password must contains alphanumeric!");
                    valid = false;
//                    Toast.makeText(RegisterActivity.this, "Password must contains alphanumeric!", Toast.LENGTH_SHORT).show();
                }

                if (Validation.isEmpty(phone))
                {
                    etPhone.setError("Phone cannot be empty!");
                    valid = false;
//					Toast.makeText(RegisterActivity.this, "Phone cannot empty!", Toast.LENGTH_SHORT).show();
                }
                else if (!Validation.phoneLength(phone))
                {
                    etPhone.setError("Phone number must be between 10 and 12 digits!");
                    valid = false;
//                    Toast.makeText(RegisterActivity.this, "Phone number must be between 10 and 12 digits!", Toast.LENGTH_SHORT).show();
                }
                else if (!Validation.isNumeric(phone))
                {
                    etPhone.setError("Phone number must be numeric!");
                    valid = false;
//                    Toast.makeText(RegisterActivity.this, "Phone number must be numeric!", Toast.LENGTH_SHORT).show();
                }

                if (Validation.isEmpty(confPassword))
                {
                    etConfPassword.setError("Confirmation password cannot be empty!");
                    valid = false;
//					Toast.makeText(RegisterActivity.this, "Confirmation Password cannot empty!", Toast.LENGTH_SHORT).show();
                }
                else if (!Validation.passwordConfirmation(password, confPassword))
                {
                    etConfPassword.setError("Confirmation password must be the same with password!");
                    valid = false;
//                    Toast.makeText(RegisterActivity.this, "Confirmation password must be the same with password!", Toast.LENGTH_SHORT).show();
                }

                if (Validation.isEmpty(dob))
                {
                    Toast.makeText(RegisterActivity.this, "Date of birth cannot empty!", Toast.LENGTH_SHORT).show();
                    valid = false;
                }
                else if(Validation.dobInTheFuture(dateOfBirth))
                {
                    Toast.makeText(RegisterActivity.this, "Date of birth cannot be in the future!", Toast.LENGTH_SHORT).show();
                    valid = false;
                }

                if (Validation.isEmpty(selectedGender))
                {
                    Toast.makeText(RegisterActivity.this, "Gender cannot empty!", Toast.LENGTH_SHORT).show();
                    valid = false;
                }

                if (!cbAgreement.isChecked())
                {
                    Toast.makeText(RegisterActivity.this, "You must agree the terms and conditions!", Toast.LENGTH_SHORT).show();
                    valid = false;
                }

                if(valid)
                {
                    Toast.makeText(RegisterActivity.this, "Register Success!", Toast.LENGTH_SHORT).show();

                    User user = new User(-1, username, password, phone, selectedGender, 0, dob);
                    usersTableHelper.insert(user);

//					Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
//					startActivity(intent);
                    finish();
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//				Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
//				startActivity(intent);
                finish();
            }
        });

        btnOpenDateDialog.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                final View dialogView = getLayoutInflater().inflate(R.layout.dialog_date, null);
                builder.setView(dialogView);

                builder.setTitle("Pick Date");
                builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        DatePicker picker = dialogView.findViewById(R.id.dpDOB);

                        String dateVal = picker.getDayOfMonth() + "/" + (picker.getMonth() + 1) + "/" + picker.getYear();
                        etDOB.setText(dateVal);
                        dateOfBirth = new Date(picker.getYear(), picker.getMonth(), picker.getDayOfMonth());
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }
}
