package com.gregorius.kuufproject;

import java.util.Calendar;
import java.util.Date;

public class Validation
{
    public static boolean isEmpty(String str)
    {
        return str.isEmpty();
    }

    public static boolean isNumeric(String str)
    {
        if (str != null && !str.trim().isEmpty())
        {
            int i, length = str.length();
            for (i = 0; i < length; i++)
            {
                if (str.charAt(i) < '0' || str.charAt(i) > '9') return false;
            }
            return true;
        }
        return false;
    }

    public static boolean usernameLength(String str)
    {
        return str.length() >= 6 && str.length() <= 12;
    }

    public static boolean passwordLength(String str)
    {
        return str.length() > 8;
    }

    public static boolean isAlphanumeric(String str)
    {
        if (str != null && !str.trim().isEmpty())
        {
            boolean containtsNumbers = false, containtsLetter = false;
            int i, length = str.length();
            for (i = 0; i < length; i++)
            {
                if (containtsNumbers && containtsLetter) break;
                else if (str.charAt(i) >= '0' && str.charAt(i) <= '9') containtsNumbers = true;
                else if ((str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') || str.charAt(i) >= 'a' && str.charAt(i) <= 'z')
                    containtsLetter = true;
            }

            return containtsLetter && containtsNumbers;
        }
        return false;
    }

    public static boolean passwordConfirmation(String str1, String str2)
    {
        return str1.equals(str2);
    }

    public static boolean phoneLength(String str)
    {
        return str.length() >= 10 && str.length() <= 12;
    }

    public static boolean dobInTheFuture(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Date now = new Date(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        return date.after(now);
    }

}
